/*
 * ublox_gps.c
 *
 *  Created on: Apr 10, 2017
 *      Author: drid
 *      Based on UPSat ADCS code
 */

#include "ublox_gps.h"
#include "hpr_datatypes.h"


static uint8_t gps_buffer[NMEA_MAX_LEN];
static uint8_t gps_sentence[NMEA_MAX_LEN];
static uint8_t gps_parser[NMEA_MAX_FIELDS][NMEA_MAX_FIELD_SIZE];

extern UART_HandleTypeDef huart1;
extern GPS_DataStoreTypeDef gps_data;

//TEMPORAL
extern uint32_t gps_test_count;

/**
 * Initialize GPS serial.
 */
void init_gps_uart() {
	/* Set up UART4 for GPS */
	memset(gps_sentence, 0, NMEA_MAX_LEN);
	HAL_UART_Receive_IT(&huart1, gps_buffer, NMEA_MAX_LEN);

	HAL_GPIO_WritePin(GPIOA,GPIO_PIN_1,GPIO_PIN_SET); //gps OFF
//	HAL_GPIO_WritePin(GPIOA,GPIO_PIN_1,GPIO_PIN_RESET); //gps ON
}

/**
 * This function handles UART interrupt request.
 * @param  huart: pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @retval None
 */
void HAL_GPS_UART_IRQHandler(UART_HandleTypeDef *huart) {
	uint32_t gps_uart_tmp1, gps_uart_tmp2;

	gps_uart_tmp1 = __HAL_UART_GET_FLAG(huart, UART_FLAG_RXNE);
	gps_uart_tmp2 = __HAL_UART_GET_IT_SOURCE(huart, UART_IT_RXNE);
	/* UART in mode Receiver ---------------------------------------------------*/
	if ((gps_uart_tmp1 != RESET) && (gps_uart_tmp2 != RESET)) {
		UART_GPS_Receive_IT(huart);
	}
}

/**
 * Receives an amount of data in non blocking mode
 * @param  huart: pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @retval HAL status
 */
void UART_GPS_Receive_IT(UART_HandleTypeDef *huart) {
	uint8_t c;

	_gps_state hpr_gps_fix;

	c = (uint8_t) (huart->Instance->DR & (uint8_t) 0x00FFU);
	if (huart->RxXferSize == huart->RxXferCount && c == '$') {
		*huart->pRxBuffPtr++ = c;
		huart->RxXferCount--;
		//start timeout
	} else if (c == '$') {
		huart->RxXferCount = huart->RxXferSize;

		*huart->pRxBuffPtr++ = c;
		huart->RxXferCount--;
		//error
		//event log
		//reset buffers & pointers
		//start timeout
	} else if (c == 0xa) {
		*huart->pRxBuffPtr++ = c;
		huart->RxXferCount--;
		*huart->pRxBuffPtr++ = 0;
		huart->pRxBuffPtr = &gps_buffer;
		memset(gps_sentence, 0, NMEA_MAX_LEN);
		for (uint8_t i = 0; i < NMEA_MAX_LEN; i++) {
			gps_sentence[i] = gps_buffer[i];
		}
		huart->RxXferCount = huart->RxXferSize;

		hpr_gps_fix.status = gps_parse_fields(&gps_sentence,
											NMEA_MAX_LEN,
											gps_parser);
		gps_parse_logic(&gps_parser, &hpr_gps_fix);

	} else if (huart->RxXferSize > huart->RxXferCount) {
		*huart->pRxBuffPtr++ = c;
		huart->RxXferCount--;
	}
	/* Error */
	if (huart->RxXferCount == 0U) {
		//huart->pRxBuffPtr = &gps;
		huart->RxXferCount = huart->RxXferSize;
	}

	gps_data.alt = hpr_gps_fix.alt;
	gps_data.lon = hpr_gps_fix.lon;
	gps_data.lat = hpr_gps_fix.lat;
	gps_data.d3fix = hpr_gps_fix.d3fix;
	gps_data.fix_timestamp = hpr_gps_fix.fix_timestamp;
	//TEMPORAL
	//gps_data.alt = 0xAAA1;
	//gps_data.lon = 0xAAAAAAA2;
	//gps_data.lat = 0xAAAAAAA3;
	//gps_data.d3fix = 0xAA;
	//gps_data.fix_timestamp = gps_test_count;
	//gps_test_count++;


}
