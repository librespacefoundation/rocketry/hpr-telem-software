
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include <bmp280_hal.h>
#include "main.h"
#include "stm32l1xx_hal.h"
#include "cmsis_os.h"
#include "fatfs.h"

/* USER CODE BEGIN Includes */
#include "hpr_ring_buffer.h"
#include "hpr_transmit.h"
#include "rfm69hw.h"
#include "ublox_gps.h"
#include "hpr_lsm9ds0_hal.h"

#define FUSE_SECONDS 5 //seconds before hpr actually starts to operate and log data.
#define ENFORCE_GROUND_MODE 0x00
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc;

I2C_HandleTypeDef hi2c1;
I2C_HandleTypeDef hi2c2;

SD_HandleTypeDef hsd;
HAL_SD_CardInfoTypedef SDCardInfo;

SPI_HandleTypeDef hspi1;

UART_HandleTypeDef huart1;

osThreadId defaultTaskHandle;
osThreadId voltageMonTaskHandle;
osThreadId imuTaskHandle;
osThreadId CoordinatorTaskHandle;
osThreadId meteoMonTaskHandle;
osThreadId gpsTaskHandle;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
//FATFS MySDFatFs;  /* File system object for SD card logical drive */
//FIL MyFile;     /* File object */
//char MySDPath[4]; /* SD card logical drive path */
osThreadId gpsUpdateTaskHandle;

osThreadId lsmTaskHandle;

HPR_RingBufferTypeDef hpr_data_store;
//IMU_DataStoreTypeDef imu_data;
GPS_DataStoreTypeDef gps_data;
BME_DataStoreTypeDef bme_data;
uint8_t battery_voltage_data;/* Holds the battery voltage in mv*/

IMU_DataStoreTypeDef imu_ring_buffer[IMU_BUFFER_LENGTH];
uint8_t imu_ring_buffer_index =0;
uint8_t rfm_tx_data[65];
uint32_t rfm_tx_count = 0;

uint8_t gps_flag =0;

uint8_t esp8266_tx_buffer[MAX_ALLOWED_LENGTH_HLDLC];

//TEMPORAL
//global counters to tset peripherals that write to ring buffer and succesully transmit.
uint32_t gps_test_count = 0;
uint16_t imu_test_count = 0;
uint32_t bme_test_count = 0;


_hpr_sensors hpr_sensors;
_hpr_sensor_status sensor_status;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC_Init(void);
static void MX_I2C1_Init(void);
static void MX_I2C2_Init(void);
static void MX_SDIO_SD_Init(void);
static void MX_SPI1_Init(void);
static void MX_USART1_UART_Init(void);
void StartDefaultTask(void const * argument);
void batteryVoltageMonitorTask(void const * argument);
void imuMeasurementTask(void const * argument);
void flightCoordinatorTask(void const * argument);
void meteoMonitorTask(void const * argument);
void gpsReceiveTask(void const * argument);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC_Init();
  MX_I2C1_Init();
  MX_I2C2_Init();
  MX_SDIO_SD_Init();
  MX_SPI1_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */


  /* initialize  rfm*/
  InitializeModule();

  /* send starting fuse sequence packet*/
  for (int pad_index = 1; pad_index < 65; pad_index+=4) {
	  rfm_tx_data[pad_index] = 0xAC;
	  rfm_tx_data[pad_index+1] = 0xAB;
	  rfm_tx_data[pad_index+2] = 0x13;
	  rfm_tx_data[pad_index+3] = 0x12;
  }
  HAL_Delay(1000);

  WriteModuleRegisterN(0x80, rfm_tx_data, 66);

  /* fuse */
  for (int fuse_index = 0; fuse_index < FUSE_SECONDS; ++fuse_index) {
	  HAL_Delay(1000);
  }

  /* send starting fuse sequence packet*/
  for (int pad_index = 1; pad_index < 65; pad_index+=4) {
	  rfm_tx_data[pad_index] = 0xDE;
	  rfm_tx_data[pad_index+1] = 0xAD;
	  rfm_tx_data[pad_index+2] = 0xBE;
	  rfm_tx_data[pad_index+3] = 0xEF;
  }

  WriteModuleRegisterN(0x80, rfm_tx_data, 66);


  /* initialize gps serial */
  init_gps_uart();

  /* initialize flight coordinator ring buffer */
  HPR_RingBufferInit(&hpr_data_store);

  /* USER CODE END 2 */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityIdle, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of voltageMonTask */
  osThreadDef(voltageMonTask, batteryVoltageMonitorTask, osPriorityNormal, 0, 128);
  voltageMonTaskHandle = osThreadCreate(osThread(voltageMonTask), NULL);

  /* definition and creation of imuTask */
  osThreadDef(imuTask, imuMeasurementTask, osPriorityNormal, 0, 256);
  imuTaskHandle = osThreadCreate(osThread(imuTask), NULL);

  /* definition and creation of CoordinatorTask */
  osThreadDef(CoordinatorTask, flightCoordinatorTask, osPriorityNormal, 0, 256);
  CoordinatorTaskHandle = osThreadCreate(osThread(CoordinatorTask), NULL);

  /* definition and creation of meteoMonTask */
  osThreadDef(meteoMonTask, meteoMonitorTask, osPriorityNormal, 0, 128);
  meteoMonTaskHandle = osThreadCreate(osThread(meteoMonTask), NULL);

  /* definition and creation of gpsTask */
  osThreadDef(gpsTask, gpsReceiveTask, osPriorityNormal, 0, 256);
  gpsTaskHandle = osThreadCreate(osThread(gpsTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */

  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
 

  /* Start scheduler */
  osKernelStart();
  
  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_5;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  RCC_OscInitStruct.PLL.PLLDIV = RCC_PLL_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

/* ADC init function */
static void MX_ADC_Init(void)
{

  ADC_ChannelConfTypeDef sConfig;

    /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
    */
  hadc.Instance = ADC1;
  hadc.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  hadc.Init.LowPowerAutoWait = ADC_AUTOWAIT_DISABLE;
  hadc.Init.LowPowerAutoPowerOff = ADC_AUTOPOWEROFF_DISABLE;
  hadc.Init.ChannelsBank = ADC_CHANNELS_BANK_A;
  hadc.Init.ContinuousConvMode = DISABLE;
  hadc.Init.NbrOfConversion = 1;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc.Init.DMAContinuousRequests = DISABLE;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_4CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* I2C1 init function */
static void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* I2C2 init function */
static void MX_I2C2_Init(void)
{

  hi2c2.Instance = I2C2;
  hi2c2.Init.ClockSpeed = 100000;
  hi2c2.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* SDIO init function */
static void MX_SDIO_SD_Init(void)
{

  hsd.Instance = SDIO;
  hsd.Init.ClockEdge = SDIO_CLOCK_EDGE_RISING;
  hsd.Init.ClockBypass = SDIO_CLOCK_BYPASS_DISABLE;
  hsd.Init.ClockPowerSave = SDIO_CLOCK_POWER_SAVE_DISABLE;
  hsd.Init.BusWide = SDIO_BUS_WIDE_1B;
  hsd.Init.HardwareFlowControl = SDIO_HARDWARE_FLOW_CONTROL_DISABLE;
  hsd.Init.ClockDiv = 0;

}

/* SPI1 init function */
static void MX_SPI1_Init(void)
{

  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART1 init function */
static void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, INEMO_EN_Pin|ESP_EN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(RBF_OUT_GPIO_Port, RBF_OUT_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPS_EN_Pin|RF_EN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, RF_NSS_Pin|BME_EN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : INEMO_EN_Pin ESP_EN_Pin */
  GPIO_InitStruct.Pin = INEMO_EN_Pin|ESP_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : RBF_OUT_Pin */
  GPIO_InitStruct.Pin = RBF_OUT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(RBF_OUT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : RBF_IN_Pin */
  GPIO_InitStruct.Pin = RBF_IN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(RBF_IN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : GPS_EN_Pin RF_EN_Pin */
  GPIO_InitStruct.Pin = GPS_EN_Pin|RF_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : RF_NSS_Pin BME_EN_Pin */
  GPIO_InitStruct.Pin = RF_NSS_Pin|BME_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : RF_INT_Pin */
  GPIO_InitStruct.Pin = RF_INT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(RF_INT_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */




/* USER CODE END 4 */

/* StartDefaultTask function */
void StartDefaultTask(void const * argument)
{
  /* init code for FATFS */
  MX_FATFS_Init();

  /* USER CODE BEGIN 5 */




	/* Infinite loop */
	for(;;)
	{

		osDelay(2000);
	}
  /* USER CODE END 5 */ 
}

/* batteryVoltageMonitorTask function */
void batteryVoltageMonitorTask(void const * argument)
{
  /* USER CODE BEGIN batteryVoltageMonitorTask */
	/* Infinite loop */
	uint32_t ADCValue;
	TickType_t xLastWakeTime;
	const TickType_t xFrequency = 1000;//unblock every 1000msec

	for (;;) {

		// Initialise the xLastWakeTime variable with the current time.
		xLastWakeTime = xTaskGetTickCount ();

		HAL_ADC_Start(&hadc);
		if (HAL_ADC_PollForConversion(&hadc, 200) == HAL_OK) {
			ADCValue = HAL_ADC_GetValue(&hadc);
			//battery_voltage_data = ADCValue * BATMON_2_mV;
			//!This scaling either NON FLOAT, or in ground reception post processing
			battery_voltage_data = (uint8_t)(ADCValue>>4);//shift from 12bit to 8bit and cast
		}
		HAL_ADC_Stop(&hadc);

		//TEMPORAL
		//battery_voltage_data = 0Xb7;


		vTaskDelayUntil(&xLastWakeTime, xFrequency);

	}
  /* USER CODE END batteryVoltageMonitorTask */
}

/* imuMeasurementTask function */
void imuMeasurementTask(void const * argument)
{
  /* USER CODE BEGIN imuMeasurementTask */
	TickType_t xLastWakeTime;
	const TickType_t xFrequency = 10;

	/* initialize imu peripheral*/
	init_hpr_lsm9ds0();

	for (;;) {

//		HAL_GPIO_WritePin(RBF_OUT_GPIO_Port, RBF_OUT_Pin, GPIO_PIN_RESET);
		// Initialise the xLastWakeTime variable with the current time.
		xLastWakeTime = xTaskGetTickCount ();

		/* Update current entry of imu ring buffer with new data.*/
//////		update_hpr_lsm9ds0_xm( &imu_ring_buffer[imu_ring_buffer_index]);
		update_hpr_lsm9ds0_gyro( &imu_ring_buffer[imu_ring_buffer_index]);
//////		update_hpr_lsm9ds0_temp( &imu_ring_buffer[imu_ring_buffer_index]);
		update_hpr_lsm9ds0_acc( &imu_ring_buffer[imu_ring_buffer_index]);

		/*Move index frwrd (check for wrapping)*/
		uint8_t new_ring_buffer_index = imu_ring_buffer_index ++;
		if(new_ring_buffer_index>=IMU_BUFFER_LENGTH){
			new_ring_buffer_index = 0;
		    /* when ring buffer is full, copy to global imu data store*/
		}
		imu_ring_buffer_index = new_ring_buffer_index;

//		HAL_GPIO_WritePin(RBF_OUT_GPIO_Port, RBF_OUT_Pin, GPIO_PIN_SET);
		vTaskDelayUntil(&xLastWakeTime, xFrequency);

	}
  /* USER CODE END imuMeasurementTask */
}

/* flightCoordinatorTask function */
void flightCoordinatorTask(void const * argument)
{
  /* USER CODE BEGIN flightCoordinatorTask */
  /* Infinite loop */
  for(;;)
  {
	  TickType_t xLastWakeTime;
	  const TickType_t xFrequency = TX_INTERVAL;

	  HAL_GPIO_WritePin(RBF_OUT_GPIO_Port, RBF_OUT_Pin, GPIO_PIN_RESET);

	  xLastWakeTime = xTaskGetTickCount ();
	  /*get pointer to fresh battery voltage data*/
	  //batMon holds the global adv battery voltage value.

	  /*get pointer to fresh IMU data*/
	  //just make sure that every IMU_BUFFER_LENGTH (10) reads, they are saved to global imu_data structure

	  /*get pointer to fresh GPS data*/
	  //At every read just save the data to global gps_data structure

	  /*get pointer to fresh BME data*/
	  //At every read just save the data to global bme_data structure

	  /*get current timeticks*/
	  TickType_t timeticks_now = xTaskGetTickCount();

	  /*Transmit gps data over rfm*/
	  HPR_RfmGpsTransmit(&hpr_data_store, rfm_tx_data);


	  /*Add new entry to ring buffer*/
	  HPR_RingBufferAddEntry(&hpr_data_store, imu_ring_buffer, &gps_data, &bme_data, battery_voltage_data,timeticks_now);

	  /*Transmit this last entry over 802.11 with esp*/
	  //HPR_EspBlockTransmit(&hpr_data_store, esp8266_tx_buffer);
//	  /*Transmit gps data over rfm*/
//	  HPR_RfmGpsTransmit(&hpr_data_store, rfm_tx_data);

	  /*If time is now (==ring buffer is full)*/
	  if( (HPR_RingBufferIsFull(&hpr_data_store)) ){

		  /*flush whole ringbuffer to non volatile memory*/
		  HPR_RingBufferSaveToSd(&hpr_data_store);
		  if(ENFORCE_GROUND_MODE){
			  //no write access to flash is allowed in ground mode
		  }
		  else{
			  //HPR_RingBufferSaveToFlash(&hpr_data_store);
		  }


//		  /*Transmit gps data over rfm*/
//		  HPR_RfmGpsTransmit(&hpr_data_store, rfm_tx_data);

	  }

	  HAL_GPIO_WritePin(RBF_OUT_GPIO_Port, RBF_OUT_Pin, GPIO_PIN_SET);
	  vTaskDelayUntil(&xLastWakeTime, xFrequency);



  }
  /* USER CODE END flightCoordinatorTask */
}

/* meteoMonitorTask function */
void meteoMonitorTask(void const * argument)
{
  /* USER CODE BEGIN meteoMonitorTask */

	TickType_t xLastWakeTime;
	const TickType_t xFrequency = 100;

	/* Infinite loop */
	for(;;)
	{
		TickType_t xLastWakeTime;
		const TickType_t xFrequency = 100;

		//HAL_GPIO_WritePin(RBF_OUT_GPIO_Port, RBF_OUT_Pin, GPIO_PIN_RESET);

		xLastWakeTime = xTaskGetTickCount ();

		/* do bme stuff here */
		uint32_t *p = &bme_data.pressure;
		uint32_t *t = &bme_data.temperature;
		bmp280_data_read(p, t);

		//TEMPORAL
		//bme_data.pressure =0xCCCCCCC1;
		//bme_data.temperature = bme_test_count;
		//bme_test_count++;


		//HAL_GPIO_WritePin(RBF_OUT_GPIO_Port, RBF_OUT_Pin, GPIO_PIN_SET);
		vTaskDelayUntil(&xLastWakeTime, xFrequency);
	}

  /* USER CODE END meteoMonitorTask */
}

/* gpsReceiveTask function */
void gpsReceiveTask(void const * argument)
{
  /* USER CODE BEGIN gpsReceiveTask */
  /* Infinite loop */
  for(;;)
  {
	  /* task has been resumed from uart gps interrupt*/
	  if(gps_flag){

		  /* do gps stuff here */
		  UART_GPS_Receive_IT(&huart1);
		  gps_flag =0;//reset back to
	  }
	  osDelay(200);
  }
  /* USER CODE END gpsReceiveTask */
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM6) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
