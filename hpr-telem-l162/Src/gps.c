/*
 * gps.c
 *
 *  Created on: Apr 10, 2017
 *      Author: drid
 *      Based on UPSat ECSS code
 */


#include "gps.h"

_gps_state gps_state = { .d3fix = 0, .lat = 0, .lon = 0, .status = GPS_DISABLE,
                         .fix_timestamp = 0};

_gps_status gps_parse_fields(uint8_t *buf, const uint8_t size,
        uint8_t (*res)[NMEA_MAX_FIELDS]) {

    uint8_t gps_crc = 0;
    uint8_t field = 0;
    uint8_t cnt = 0;

    for (uint8_t i = 0; i < size; i++) {

        gps_crc = gps_crc ^ buf[i];

        if (buf[i] == ',') {

            res[field++][cnt] = 0;
            cnt = 0;

        } else if (buf[i] == '*') {

            res[field][cnt] = 0;

			uint8_t res_crc = strtol(&buf[i + 1], buf[i + 2], 16);
			if (gps_crc != res_crc) {
				return GPS_ERROR;
			} else {
				return GPS_NORMAL;
			}

        } else {
            res[field][cnt++] = buf[i];
        }

    }

    return GPS_NORMAL;
}

_gps_status gps_parse_logic(const uint8_t (*res)[NMEA_MAX_FIELDS],
        _gps_state *state) {

    /*
     $GPGSA,M,3,31,32,22,24,19,11,17,14,20,,,,1.6,0.9,1.3*3E

     GSA Satellite status
     M Auto selection of 2D or 3D fix (M = manual)
     3 3D fix - values include: 1 = No Fix
     2 = 2D Fix
     3 = 3D Fix
     31,32... PRNs of satellites used for Fix (space for 12)
     1.6 Position Dilution of Precision (PDOP)
     0.9 Horizontal Dilution of Precision (HDOP)
     1.3 Vertical Dilution of Precision (VDOP)
     *3e The checksum data, always begin with *
     */

    if (strncmp("$GPGSA", &res[NMEA_HEADER][0], NMEA_HEADER_SIZE) == 0) {
    	state->d3fix = strtol(&res[NMEA_GSA_3DFIX][0],
                &res[NMEA_GSA_3DFIX][NMEA_MAX_FIELD_SIZE], 10);
    }

    /*
     GGA - Fix Data - The NMEA SENTENCE
     $GPGGA,172120.384,5219.0671,N,05117.0926,E,1,9,0.9,371262.1,M,0,M,,,*54
     Global Positioning System Fix Data
     Fix taken at 17:21:20.384 UTC
     Latitude 52 deg 19.0671' N
     Longitude 51 deg 17.0926' E
     Fix quality:
     0 = Invalid
     1 = GPS Fix (SPS)
     Number of satellites being tracked
     Horizontal Dilution of Precision (HDOP)
     Altitude, meters, above WGS84 ellipsoid 1
     Height of the Geoid (mean sea level) above WGS84 ellipsoid
     Time in seconds since last DGPS update
     DGPS station ID number
     The checksum data, always begin with *
     */

	else if (strncmp("$GPGGA", &res[NMEA_HEADER][0], NMEA_HEADER_SIZE) == 0) {
		state->fix_timestamp = strtol(&res[NMEA_GGA_TIME][0],
				&res[NMEA_GGA_TIME][NMEA_MAX_FIELD_SIZE], 10);

//        state->_3dfix = strtoi(&res[NMEA_GPS_GGA_FIX][0],
//                &res[NMEA_GGA_NUM_SAT][NMEA_MAX_FIELD_SIZE]);

		state->lat = strtof(&res[NMEA_GPS_GGA_LAT][0],
				&res[NMEA_GGA_NUM_SAT][NMEA_MAX_FIELD_SIZE]);
		if (res[NMEA_GPS_GGA_NS][0] == 'S')
			state->lat *= -1;

		state->lon = strtof(&res[NMEA_GPS_GGA_LON][0],
				&res[NMEA_GGA_NUM_SAT][NMEA_MAX_FIELD_SIZE]);
		if (res[NMEA_GPS_GGA_NS][0] == 'W')
			state->lon *= -1;

		state->alt = strtof(&res[NMEA_GPS_GGA_ALT][0],
				&res[NMEA_GGA_NUM_SAT][NMEA_MAX_FIELD_SIZE]);
	}

    return GPS_NORMAL;

}
