/*
 * hpr_transmit.h
 *
 *  Created on: Apr 10, 2017
 *      Author: ariolis
 */

#ifndef HPR_TRANSMIT_H_
#define HPR_TRANSMIT_H_

#include "hpr_ring_buffer.h"

#define ESP8266_TXBUFFERSIZE 255
#define MAX_ALLOWED_LENGTH_HLDLC 102
#define ESP8266_START_FLAG '$'
#define ESP8266_STOP_FLAG  '*'


void HPR_EspBlockTransmit(HPR_RingBufferTypeDef *ring_buffer, uint8_t *tx_buffer);

void HPR_RfmGpsTransmit(HPR_RingBufferTypeDef *ring_buffer, uint8_t *rfm_data);

uint8_t HLDLC_frame(uint8_t *input_buffer, uint8_t *output_buffer, uint16_t *buffer_size);

#endif /* HPR_TRANSMIT_H_ */
