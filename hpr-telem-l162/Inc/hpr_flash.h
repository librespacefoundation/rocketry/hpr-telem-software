/*
 * hpr_flash.h
 *
 *  Created on: Apr 18, 2017
 *      Author: ariolis
 */
#include "stm32l1xx_hal.h"

#ifndef HPR_FLASH_H_
#define HPR_FLASH_H_



#endif /* HPR_FLASH_H_ */

//2 pages of 192Kbytes = 384Kbytes in total
#define HPR_FLASH_START_ADRESS 0x08000000
#define HPR_FLASH_START_OFFSET 0x00000004
#define HPR_FLASH_DATASTORE_START_ADRESS HPR_FLASH_START_ADRESS + HPR_FLASH_START_OFFSET
#define HPR_FLASH_DATASTORE_STOP_ADRESS 0x08030000

static uint8_t flash_memory_write_status = 0xFF;
static uint32_t flash_memory_write_adress = HPR_FLASH_DATASTORE_START_ADRESS;

/**
 * @brief rbf status.
 *
 * This datatype is used to define the status of hpr rbf connector.
 * This connector connects gpio h0 to gpio h1 via a 1Kohm resistor while in ground mode.
 * Stays disconnected while in flight mode.
 */
typedef enum {
	HPR_RBF_FLIGHT_MODE,/**< hpr in flight mode - data are saved to flash memory  */
	HPR_RBF_GROUND_MODE,/**< hpr in ground mode - data are saved to flash memory */
	HPR_RBF_UNDEFINED,/**< hpr state droped in unresolved situation */
	HPR_RBF_LAST_VALUE/**< hpr state non valid. */
}HPR_rbf_status;

HPR_rbf_status HPR_get_rbf_status(void);

void HPR_get_memory_word(uint32_t memory_address, uint32_t *memory_data );

void HPR_save_to_flash_memory_word(uint32_t *memory_data);
