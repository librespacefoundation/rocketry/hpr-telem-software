#ifndef __HPR_DATATYPES_H
#define __HPR_DATATYPES_H

#include "stdint.h"

#define IMU_BUFFER_LENGTH 10 // This is the amount of imu vectors that will be stored for each period.
                             // For this case, we are collecting data every 10 msec, and handling data
                             // every 100 msec, thus IMU_BUFFER_LENGTH = 10

#define HPR_DUMMY_DATA_8 0xF1
#define HPR_DUMMY_DATA_16 0xACAB
#define HPR_DUMMY_DATA_32 0x1312ACAB

typedef struct
{
  uint16_t magnetometer_x_axis_raw;       /*!< Value1 of IMU sensor description to be stored and
                              transmitted. */

  uint16_t magnetometer_y_axis_raw;       /*!< Value2 of IMU sensor description to be stored and
                              transmitted. */

  uint16_t magnetometer_z_axis_raw;       /*!< Value3 of IMU sensor description to be stored and
                              transmitted. */

  uint16_t gyroscope_x_axis_raw;       /*!< Value4 of IMU sensor description to be stored and
                              transmitted. */

  uint16_t gyroscope_y_axis_raw;       /*!< Value5 of IMU sensor description to be stored and
                              transmitted. */

  uint16_t gyroscope_z_axis_raw;       /*!< Value6 of IMU sensor description to be stored and
                              transmitted. */

  int16_t accelerometer_x_axis_raw;       /*!< Value7 of IMU sensor description to be stored and
                              transmitted. */

  int16_t accelerometer_y_axis_raw;       /*!< Value8 of IMU sensor description to be stored and
                              transmitted. */

  int16_t accelerometer_z_axis_raw;       /*!< Value9 of IMU sensor description to be stored and
                              transmitted. */

  int16_t temperature_raw;       /*!< Value10 of IMU sensor description to be stored and
                              transmitted. */

}IMU_DataStoreTypeDef;


typedef struct
{
	float lat;       /*!< Value1 of GPS sensor description to be stored and
                              transmitted. */

	float lon;       /*!< Value2 of GPS sensor description to be stored and
                              transmitted. */

	int16_t alt;       /*!< Value3 of GPS sensor description to be stored and
                              transmitted. */

	uint8_t d3fix;       /*!< Value4 of GPS sensor description to be stored and
                              transmitted. */

	uint32_t fix_timestamp;       /*!< Value5 of GPS sensor description to be stored and
                              transmitted. */

}GPS_DataStoreTypeDef;

typedef struct
{
  uint32_t pressure;       /*!< Pressure calibrated. */

  int32_t temperature;       /*!< temperature calibrated. */

  uint32_t humidity;       /*!< humidity calibrated. */


}BME_DataStoreTypeDef;


typedef struct
{
	IMU_DataStoreTypeDef imu_datastore[IMU_BUFFER_LENGTH];/*!< Value1 of BME sensor description
	                                                           to be stored and transmitted. */

	GPS_DataStoreTypeDef gps_datastore;       /*!< Value2 of BME sensor description to be stored and
                                            transmitted. */

	BME_DataStoreTypeDef bme_datastore;       /*!< Value3 of BME sensor description to be stored and
                                            transmitted. */

	uint8_t battery_voltage;    /*hpr battery voltage  monitor*/

	uint32_t time_ticks;  /*lets keep here a pseudo timestamp for each log depending on number of
	                        timeticks when the data are packetized to the ringbuffer entry.*/


}HPR_DataStoreTypeDef;










#endif
