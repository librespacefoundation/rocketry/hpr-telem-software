/*
 * gps.h
 *
 *  Created on: Apr 10, 2017
 *      Author: drid
 *      Based on UPSat ECSS code
 */

#ifndef __GPS_H
#define __GPS_H

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

/** UBLOX sentences
 * GSA
 * GSV
 * GLL */

/** GGA          Global Positioning System Fix Data
     123519       Fix taken at 12:35:19 UTC
     4807.038,N   Latitude 48 deg 07.038' N
     01131.000,E  Longitude 11 deg 31.000' E
     1            Fix quality: 0 = invalid
                               1 = GPS fix (SPS)
                               2 = DGPS fix
                               3 = PPS fix
			       4 = Real Time Kinematic
			       5 = Float RTK
                               6 = estimated (dead reckoning) (2.3 feature)
			       7 = Manual input mode
			       8 = Simulation mode
     08           Number of satellites being tracked
     0.9          Horizontal dilution of position
     545.4,M      Altitude, Meters, above mean sea level
     46.9,M       Height of geoid (mean sea level) above WGS84
                      ellipsoid
     (empty field) time in seconds since last DGPS update
     (empty field) DGPS station ID number
     *47          the checksum data, always begins with * */

/* RMC          Recommended Minimum sentence C
     123519       Fix taken at 12:35:19 UTC
     A            Status A=active or V=Void.
     4807.038,N   Latitude 48 deg 07.038' N
     01131.000,E  Longitude 11 deg 31.000' E
     022.4        Speed over the ground in knots
     084.4        Track angle in degrees True
     230394       Date - 23rd of March 1994
     003.1,W      Magnetic Variation
     *6A          The checksum data, always begins with *
 * VTG */
#define NMEA_GPS_GGA_LAT	2
#define NMEA_GPS_GGA_NS		3
#define NMEA_GPS_GGA_LON	4
#define NMEA_GPS_GGA_EW		5
#define NMEA_GPS_GGA_ALT	9
#define NMEA_GPS_GGA_FIX	6
#define NMEA_GPS_GGA_TIME	1

#define NMEA_MAX_FIELD_SIZE 15
#define NMEA_MAX_FIELDS     22
#define NMEA_HEADER         0
#define NMEA_HEADER_SIZE    6
#define NMEA_MAX_LEN        80
#define NMEA_NUM_SENTENCES  8

#define NMEA_GSA_3DFIX      2

#define NMEA_GPS_TIME       1
#define NMEA_GPS_WEEK       2

#define NMEA_GGA_TIME       1
#define NMEA_GGA_NUM_SAT    7

typedef enum {
    GPS_ERROR = 0,
    GPS_NORMAL,
    GPS_ENABLE,
    GPS_DISABLE
} _gps_status;

typedef struct {
	float lat;
	float lon;
	int16_t alt;
	uint8_t d3fix;
	uint32_t fix_timestamp;
    _gps_status status;
} _gps_state;

union _cnv {
    double cnvD;
    float cnvF;
    uint32_t cnv32;
    uint16_t cnv16[4];
    uint8_t cnv8[8];
};

_gps_status gps_parse_fields(uint8_t *buf, const uint8_t size,
        uint8_t (*res)[NMEA_MAX_FIELDS]);

_gps_status gps_parse_logic(const uint8_t (*res)[NMEA_MAX_FIELDS],
        _gps_state *state);

#endif
